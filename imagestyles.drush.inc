<?php

/**
 * Implements hook_drush_command().
 */
function imagestyles_drush_command() {
  $items = array();

  $items['imagestyles'] = array(
    'callback' => 'imagestyles_drush_callback',
    'description' => "Display a list of all image styles.",
  );
  return $items;
}

/**
 * Implements hook_drush_help().
 */
function imagestyles_drush_help($section) {
  switch ($section) {
    case 'drush:imagestyles':
      return dt("Displays a list of all image styles. Example: drush imagestyles");
  }
}

/**
 * Drush callback: Prints out an array of image styles and their settings.
 */
function imagestyles_drush_callback() {
  $styles = image_styles();

  foreach ($styles as $style) {
    print "\n\n" . $style['name'] . "\n";
    print_r($style['effects']);
  }
}

